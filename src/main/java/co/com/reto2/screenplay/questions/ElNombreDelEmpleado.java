package co.com.reto2.screenplay.questions;

import co.com.reto2.screenplay.ui.OrangeHRSearchPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Visibility;

public abstract class ElNombreDelEmpleado implements Question<Boolean> {

	
	@Override
	public Boolean answeredBy(Actor actor) {
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
			
		}
		return Visibility.of(OrangeHRSearchPage.NAME_SEARCH).viewedBy(actor).asBoolean();
	}

		
	public static ElNombreDelEmpleado EstePresente() {
		
		return new ElNombreDelEmpleado() {
			
		};
	}



}
