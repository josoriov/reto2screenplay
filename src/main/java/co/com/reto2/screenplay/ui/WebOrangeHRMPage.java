package co.com.reto2.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")
public class WebOrangeHRMPage extends PageObject{

	public static final Target BOTON_LOGGIN = Target.the("Bot�n Loggin").located(By.id("btnLogin"));
	public static final Target BOTON_PIM = Target.the("Bot�n PIM").located(By.xpath("//*[@id=\'menu_pim_viewPimModule\']/a"));
	public static final Target BOTON_AGREGAR_EMPLEADO = Target.the("Bot�n agregar empleado").located(By.id("menu_pim_addEmployee"));
	public static final Target CAMPO__PRIMER_NOMBRE = Target.the("Primer Nombre").located(By.id("firstName"));
	public static final Target CAMPO_SEGUNDO_NOMBRE = Target.the("Segundo Nombre").located(By.id("middleName"));
	public static final Target CAMPO_APELLIDO = Target.the("Apellidos").located(By.id("lastName"));
	public static final Target CAMPO_ID_EMPLEADO = Target.the("Identificaci�n").located(By.id("employeeId"));
	public static final Target SELECT_OPTION_LOCACION = Target.the("Locaci�n").located(By.xpath("//*[@id=\'location_inputfileddiv\']/div/input"));
	public static final Target LOCATION_LIST = Target.the("Location employee").located(By.id("location_inputfileddiv"));
	public static final Target LOCATION = Target.the("Location employee").located(By.xpath("//*[@id='location_inputfileddiv']/div/input"));
	public static final Target BOTON_SAVE = Target.the("Location employee").located(By.id("systemUserSaveBtn"));
	public static final Target CAMPO_OTHER_ID = Target.the("Other Id of employee").located(By.id("other_id"));
	public static final Target CAMPO_DATE_OF_BIRTH = Target.the("Date of birth of employee").located(By.id("date_of_birth"));
	public static final Target CAMPO_MARITAL_STATUS = Target.the("Marital status of employee").located(By.id("marital_status_inputfileddiv"));
	public static final Target CHECK_GENDER_MALE = Target.the("Radio button gender male").located(By.xpath("//LABEL[@ng-class='{disabled: form.readonly}'][text()='Male']"));
	public static final Target CHECK_GENDER_FEMALE = Target.the("Radio button gender female").located(By.xpath("//LABEL[@ng-class='{disabled: form.readonly}'][text()='Female']"));
	public static final Target CAMPO_DRIVER_LICENSE = Target.the("Number of driver license").located(By.id("driver_license"));
	public static final Target SELECT_NATIONALITY = Target.the("Nationality of employee").located(By.id("nationality_inputfileddiv"));
	public static final Target CAMPO_LICENSE_EXPIRY_DATE= Target.the("License expiry date").located(By.id("license_expiry_date"));
	public static final Target CAMPO_NICKNAME= Target.the("Nickname").located(By.id("nickName"));
	public static final Target CAMPO_MILITARY_SERVICE = Target.the("Military service").located(By.id("militaryService"));
	public static final Target CHECK_SMOKER = Target.the("CheckBox smoker").located(By.xpath("//LABEL[@for='smoker'][text()='Smoker']"));
	public static final Target BOTON_SAVE_PERSONAL_DETAILS = Target.the("Button of saver personal details").located(By.xpath("//BUTTON[@type='submit'][text()='Save']"));

	
	
}

