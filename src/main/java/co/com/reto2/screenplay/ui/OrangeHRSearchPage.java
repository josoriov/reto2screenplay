package co.com.reto2.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class OrangeHRSearchPage extends PageObject {

	public static final Target BUTTON_EMPLOYEE_LIST = Target.the("Bot�n de Menu Listado de Empleados").located(By.id("menu_pim_viewEmployeeList"));
	public static final Target EMPLOYEE_NAME = Target.the("Campo Nombre ").located(By.id("employee_name_quick_filter_employee_list_value"));
	public static final Target BUTTON_SEARCH = Target.the("Bot�n Buscar").located(By.id("quick_search_icon"));
	public static final Target NAME_SEARCH = Target.the("Resultado de b�squeda").located(By.xpath("//*[@id='employeeListTable']/tbody/tr[1]"));
	
}
	