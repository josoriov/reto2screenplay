package co.com.reto2.screenplay.tasks;

import java.util.List;

import org.openqa.selenium.Keys;

import co.com.reto2.screenplay.ui.OrangeHRSearchPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Buscar implements Task{

	private List<List<String>> dataEmpleado;
	
	public Buscar(List<List<String>> dataEmpleado) {
		super();
		this.dataEmpleado = dataEmpleado;
	}
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		try {
			String Name =dataEmpleado.get(1).get(0).trim()+ ' '+ dataEmpleado.get(1).get(1).trim()+' '+ dataEmpleado.get(1).get(2);
			Thread.sleep(3000);
			actor.attemptsTo(
					Click.on(OrangeHRSearchPage.BUTTON_EMPLOYEE_LIST),
					Enter.theValue(Name).into(OrangeHRSearchPage.EMPLOYEE_NAME).thenHit(Keys.ENTER),
					Click.on(OrangeHRSearchPage.BUTTON_SEARCH)
			);
		} catch (Exception e) {
			e.getMessage();
		}
		
	}

	public static Buscar ElEmpleadoRegistrado(List<List<String>> dataEmpleado) {
		// TODO Auto-generated method stub
		return Tasks.instrumented(Buscar.class, dataEmpleado);
	}

}
