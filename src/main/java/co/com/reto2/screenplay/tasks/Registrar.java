package co.com.reto2.screenplay.tasks;

import co.com.reto2.screenplay.interactions.Select;
import co.com.reto2.screenplay.ui.WebOrangeHRMPage;
import cucumber.api.DataTable;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

import java.util.List;

import org.openqa.selenium.support.ui.Wait;


import net.serenitybdd.screenplay.waits.WaitUntil;

public class Registrar implements Task {
	
	public Registrar(List<List<String>> datosEmpleado) {
		super();
		this.datosEmpleado = datosEmpleado;
	}

	private List<List<String>> datosEmpleado;

	@Override
	public <T extends Actor> void performAs(T actor) {
		try{
		actor.attemptsTo(
			Click.on(WebOrangeHRMPage.BOTON_PIM),
			Click.on(WebOrangeHRMPage.BOTON_AGREGAR_EMPLEADO));
			Thread.sleep(4000);
			actor.attemptsTo(
			Enter.theValue(datosEmpleado.get(1).get(0).trim()).into(WebOrangeHRMPage.CAMPO__PRIMER_NOMBRE),	
			Enter.theValue(datosEmpleado.get(1).get(1).trim()).into(WebOrangeHRMPage.CAMPO_SEGUNDO_NOMBRE),
			Enter.theValue(datosEmpleado.get(1).get(2).trim()).into(WebOrangeHRMPage.CAMPO_APELLIDO),
			Enter.theValue(datosEmpleado.get(1).get(3).trim()).into(WebOrangeHRMPage.CAMPO_ID_EMPLEADO),
			Click.on(WebOrangeHRMPage.LOCATION),				
			Select.ofTheList(WebOrangeHRMPage.LOCATION_LIST, datosEmpleado.get(1).get(4).trim()),
			Click.on(WebOrangeHRMPage.BOTON_SAVE)
		);
		Thread.sleep(4000);
		actor.attemptsTo(
		Enter.theValue(datosEmpleado.get(1).get(5).trim()).into(WebOrangeHRMPage.CAMPO_OTHER_ID),		
		Enter.theValue(datosEmpleado.get(1).get(6).trim()).into(WebOrangeHRMPage.CAMPO_DATE_OF_BIRTH),
		Click.on(WebOrangeHRMPage.CAMPO_MARITAL_STATUS),
		Select.ofTheList(WebOrangeHRMPage.CAMPO_MARITAL_STATUS, datosEmpleado.get(1).get(7).trim()),
		Click.on(WebOrangeHRMPage.SELECT_NATIONALITY),
		Select.ofTheList(WebOrangeHRMPage.SELECT_NATIONALITY, datosEmpleado.get(1).get(8).trim()),
		Enter.theValue(datosEmpleado.get(1).get(9).trim()).into(WebOrangeHRMPage.CAMPO_DRIVER_LICENSE),
		Enter.theValue(datosEmpleado.get(1).get(10).trim()).into(WebOrangeHRMPage.CAMPO_LICENSE_EXPIRY_DATE),
		Enter.theValue(datosEmpleado.get(1).get(11).trim()).into(WebOrangeHRMPage.CAMPO_NICKNAME),
		Enter.theValue(datosEmpleado.get(1).get(12).trim()).into(WebOrangeHRMPage.CAMPO_MILITARY_SERVICE)
		);
		if(datosEmpleado.get(1).get(13).trim().equals("Male"))
			actor.attemptsTo(Click.on(WebOrangeHRMPage.CHECK_GENDER_MALE));
		else
			actor.attemptsTo(Click.on(WebOrangeHRMPage.CHECK_GENDER_FEMALE));
		
		if(datosEmpleado.get(1).get(13).trim().equals("Yes"))
		{
			actor.attemptsTo(Click.on(WebOrangeHRMPage.CHECK_SMOKER));
		}
		actor.attemptsTo(Click.on(WebOrangeHRMPage.BOTON_SAVE_PERSONAL_DETAILS));
		}catch(Exception e){
			System.out.println("Error in the Registrar Task");
		}
	}

	public static Registrar LosDatosDeElEmpleadoEnElFormulario(List<List<String>> dataEmpleado) {	
		return Tasks.instrumented(Registrar.class, dataEmpleado);
	}

}
