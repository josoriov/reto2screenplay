package co.com.reto2.screenplay.tasks;

import co.com.reto2.screenplay.ui.WebOrangeHRMPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task {

	private WebOrangeHRMPage webOrangeHRMPage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(webOrangeHRMPage),
		Click.on(WebOrangeHRMPage.BOTON_LOGGIN)		
		);
	}

	public static Abrir LaPaginaOrangeHRM() {	
		return Tasks.instrumented(Abrir.class);
	}

}
