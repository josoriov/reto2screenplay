package co.com.reto2.screenplay.stepdefinitions;


import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.reto2.screenplay.questions.ElNombreDelEmpleado;
import co.com.reto2.screenplay.tasks.Abrir;
import co.com.reto2.screenplay.tasks.Buscar;
import co.com.reto2.screenplay.tasks.Registrar;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class WebOrangeHRMStepDefinitions {



	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor juan = Actor.named("Juan");
	
	@Before
	public void configuracionInicial() {
	 juan.can(BrowseTheWeb.with(hisBrowser));	
	}
	
	@Dado("^que Juan necesita crear un empleado en el OrageHR$")
	public void queJuanNecesitaCrearUnEmpleadoEnElOrageHR() throws Exception {
	    juan.wasAbleTo(Abrir.LaPaginaOrangeHRM());;
	}


	@Cuando("^el realiza el ingreso del registro en la aplicación$")
	public void elRealizaElIngresoDelRegistroEnLaAplicación(DataTable DatosEmpleado) throws Exception {
		List<List<String>> dataEmpleado = DatosEmpleado.raw();
		juan.attemptsTo(Registrar.LosDatosDeElEmpleadoEnElFormulario(dataEmpleado), Buscar.ElEmpleadoRegistrado(dataEmpleado));
	}	

	@Entonces("^el visualiza el nuevo empleado en el aplicativo$")
	public void elVisualizaElNuevoEmpleadoEnElAplicativo() throws Exception {
		juan.should(GivenWhenThen.seeThat(ElNombreDelEmpleado.EstePresente()));
	}
	
}
