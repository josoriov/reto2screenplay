#Author: jucosori@bancolombia.com.co
#language:es

@tag
Característica: Web OrageHR Interprise
Como usuario
Quiero ingresar a la OrageHRM  
para crear un nuevo usuario en el aplicativo

@tag1
Escenario: Registrar empleado en la pagina OrangeHRM Interprise
Dado que Juan necesita crear un empleado en el OrageHR
Cuando el realiza el ingreso del registro en la aplicación
|<Nombre>	|<Nombre2>|<Apellidos>|<ID>	|<Location>							|<OtroID>	|<Cumpleaños>	|<Estatus>|<Nacionalidad>	|<Licencia>	|<Expiración>	|<apodo>|<Militar> 	|<Genero>	|<Fuma> 	|
|juan 		|	carlos	|Osorio			|54646|Australian Regional HQ	|31834		|1993-05-10		|	Married |Colombian			|	5665465		|2020-10-30		|juancho|131231331	|Male			|No				|
Entonces el visualiza el nuevo empleado en el aplicativo
